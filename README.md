# mingw-w64 test project

Simple test of cross-compiling Windows 32 & 64 bit executables with 
mingw-w64 on Debian/Ubuntu Linux.

You need to have mingw-w64 and make installed:
```
sudo apt-get install mingw-w64 make
```

To build run:
```
make
```

Binaries should appear in `bin/`.

Enjoy.

